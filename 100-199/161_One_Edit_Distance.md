```java
class Solution {
    public boolean isOneEditDistance(String s, String t) {
        if(s == null || t == null) return false;
        if(s.length() < t.length()) return isOneEditDistance(t, s);
        if(s.length() - t.length() > 1) return false;
        
        boolean sameLen = (s.length() == t.length());
        
        for(int i = 0; i < t.length(); i++) {
            if(s.charAt(i) != t.charAt(i)) {
                if(!sameLen) {
                    return s.substring(i+1).equals(t.substring(i));
                } else {
                    return s.substring(i+1).equals(t.substring(i+1));
                }
            }
        }
        
        //  every single character in t.length() is equal
        return !sameLen;
    }
}
```

