DP problem. If char S[i] is '(', the substring is not valid, dp[i] = 0; else if S[i] is ')', go to find backward if there is a '(' before valid substring between them, if true, validate S[i] and the '(', dp[i] = dp[i-1]+2+ the valid substring length before the '('. 

Alternative solution: stack.



```Java
class Solution {
    public int longestValidParentheses(String s) {
        if(s.length() < 2) return 0;
        int max = 0;
        int[] dp = new int[s.length()];
        
        for(int i = 1; i < dp.length; i++)
            if(s.charAt(i) == ')' && i - dp[i-1]-1 >= 0 && s.charAt(i - dp[i-1] - 1) == '(') {
                dp[i] = dp[i-1] + 2 + ((i - dp[i-1] - 2) >= 0 ? dp[i-dp[i-1]-2] : 0);
                max = max > dp[i] ? max : dp[i];
            }
        
        return max;
    }
}
```

