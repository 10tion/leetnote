# KMP

Typical KMP algorithm codes: time complexity O(n), n is length of string s

```java
class Solution {
    // @param s the string to be searched whether a pattern is in it
    // @param p the pattern string
    // @return the index of first occurrence of p in s, otherwise -1 if no occurrence
    public int firstIndex(String s, String p) {
        int[] next = generate(p);
        int k = -1;
        for(int i = 0; i < s.length(); i++) {
            while(k > -1 && s.charAt(i) != p.charAt(k + 1))
                k = next[k];

            if(s.charAt(i) == p.charAt(k + 1))
                k++;

            if(k == p.length() - 1)
                return i - p.length() + 1;
        }

        return -1;
    }

    // generate next array for pattern string
    private int[] generate(String p) {
        int[] next = new int[p.length()];
        char[] pc = p.toCharArray();

        next[0] = -1;
        int k = -1;
        for (int q = 1; q < p.length(); q++) {
            while(k > -1 && pc[q] != pc[k + 1])
                k = next[k];
            if(pc[q] == pc[k + 1])
                k++;
            next[q] = k;
        }
        return next;
    }
}
```

