# 401. Binary Watch

A binary watch has 4 LEDs on the top which represent the **hours** (**0-11**), and the 6 LEDs on the bottom represent the **minutes** (**0-59**).

Each LED represents a zero or one, with the least significant bit on the right.

For example, the above binary watch reads "3:25".

Given a non-negative integer *n* which represents the number of LEDs that are currently on, return all possible times the watch could represent.

**Example:** 

```
Input: n = 1
Return: ["1:00", "2:00", "4:00", "8:00", "0:01", "0:02", "0:04", "0:08", "0:16", "0:32"]
```

**Note:**

- The order of output does not matter.
- The hour must not contain a leading zero, for example "01:00" is not valid, it should be "1:00".
- The minute must be consist of two digits and may contain a leading zero, for example "10:2" is not valid, it should be "10:02".



Not an easy problem. Should take care. At least it costs me much time at 1st time.

Backtracking. Optimized from [discussion](https://leetcode.com/problems/binary-watch/discuss/88456/3ms-Java-Solution-Using-Backtracking-and-Idea-of-%22Permutation-and-Combination%22)

```java
class Solution {
    public List<String> readBinaryWatch(int num) {
        List<String> res = new ArrayList<String>();
        int[] dict = new int[]{1, 2, 4, 8, 16, 32};
        
        for(int i = 0; i <= num; i++) {
            List<Integer> hours = generateDigits(dict, 4, i);
            List<Integer> minus = generateDigits(dict, 6, num - i);
            for(int hour: hours) {
                if(hour >= 12) continue;
                for(int minu : minus) {
                    if(minu >= 60) continue;
                    res.add(hour + ":" + (minu < 10 ? "0" : "") + minu);
                }
            }
        }
        
        return res;
    }
    
    private List<Integer> generateDigits(int[] dict, int digits, int count) {
        List<Integer> res = new ArrayList<Integer>();
        generateHelper(dict, digits, count, 0, 0, res);
        return res;
    }
    
    private void generateHelper(int[] dict, int digits, int count, int pos, int sum, List<Integer> res) {
        if(count == 0) {
            res.add(sum);
            return;
        }
        
        for(int i = pos; i < digits; i++) {
            generateHelper(dict, digits, count - 1, i + 1, sum + dict[i], res);
        }
    }
}
```



Brutal Force solution, O(1) && O(1)

```java
public List<String> readBinaryWatch(int num) {
    List<String> times = new ArrayList<>();
    for (int h=0; h<12; h++)
        for (int m=0; m<60; m++)
            if (Integer.bitCount(h * 64 + m) == num)
                times.add(String.format("%d:%02d", h, m));
    return times;        
}
```

