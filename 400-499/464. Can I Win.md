# 464. Can I Win

In the "100 game," two players take turns adding, to a running total, any integer from 1..10. The player who first causes the running total to reach or exceed 100 wins.

What if we change the game so that players cannot re-use integers?

For example, two players might take turns drawing from a common pool of numbers of 1..15 without replacement until they reach a total >= 100.

Given an integer `maxChoosableInteger` and another integer `desiredTotal`, determine if the first player to move can force a win, assuming both players play optimally.

You can always assume that `maxChoosableInteger` will not be larger than 20 and `desiredTotal` will not be larger than 300.

**Example**

```
Input:
maxChoosableInteger = 10
desiredTotal = 11

Output:
false

Explanation:
No matter which integer the first player choose, the first player will lose.
The first player can choose an integer from 1 up to 10.
If the first player choose 1, the second player can only choose integers from 2 up to 10.
The second player will win by choosing 10 and get a total = 11, which is >= desiredTotal.
Same with other integers chosen by the first player, the second player will always win.
```



Similar to 294. Flip Game II, DFS problem.

The key point is: if a player can `force a win`, it means if the player pick a number and after that the other player cannot `force a win` whatever he picks. Once the number exist the result is `true`.



In this problem if we do not use memo, the time complexity will be O(n!). Otherwise if we used memo, the time complexity should be O(2^n) because we generate all possibility of combination. The numbers chosed state should be keys. But we cannot store array since in java array is stored as reference. The `maxChoosableInteger` < 20, so we can take a bitmap.



```java
class Solution {
    public boolean canIWin(int maxChoosableInteger, int desiredTotal) {
        if(maxChoosableInteger >= desiredTotal) return true;
        if((1 + maxChoosableInteger) * maxChoosableInteger / 2 < desiredTotal) return false;
        
        // boolean[] used = new boolean[maxChoosableInteger + 1];
        int used = 0;
        HashMap<Integer, Boolean> map = new HashMap<>();
        
        return dfs(used, map, desiredTotal, maxChoosableInteger);
    }
    
    private boolean dfs(int used, Map<Integer, Boolean> map, int desiredTotal, int length) {
        if(desiredTotal <= 0) return false;
        if(map.containsKey(used)) return map.get(used);
        
        for(int i = 1; i <= length; i++) {
            int cur = 1 << (i - 1);
            if((cur & used) == 0) {
                boolean res = dfs(used | cur, map, desiredTotal - i, length);
                if(!res) {
                    map.put(used, true);
                    return true;
                }
            }
        }
            
        return false;
    }
}
    
    /*
    
    private boolean dfs(boolean[] used, Map<Integer, Boolean> map, int desiredTotal) {
        if(desiredTotal <= 0) return false;
        int key = transform(used);
        if(map.containsKey(key)) return map.get(key);
        
        for(int i = 1; i < used.length; i++) {
            if(!used[i]) {
                used[i] = true;
                boolean res = dfs(used, map, desiredTotal - i);
                used[i] = false;
                if(!res) {
                    map.put(key, true);
                    return true;
                }
            }
        }
        
        return false;
    }
    
    private int transform(boolean[] used) {
        int num = 0;
        for(boolean b: used){
            num <<= 1;
            if(b) num |= 1;
        }
        return num;
    }
    
}
*/
```



There are many super fast solutions in the standard AC codes. All of them are make tricks to reduce hashmap costs: use array(or bitmap integer) to store the key-value pairs, of which index is the key. However, on worst case the space needed is 2^(MaxChoosableInteger) = 2^20 = 4 Kbit = 512 Bytes (assume use bitmap). An ugly implementation.