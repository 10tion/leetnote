#490. The Maze

There is a **ball** in a maze with empty spaces and walls. The ball can go through empty spaces by rolling **up**, **down**, **left** or **right**, but it won't stop rolling until hitting a wall. When the ball stops, it could choose the next direction.

Given the ball's **start position**, the **destination** and the **maze**, determine whether the ball could stop at the destination.

The maze is represented by a binary 2D array. 1 means the wall and 0  means the empty space. You may assume that the borders of the maze are  all walls. The start and destination coordinates are represented by row  and column indexes.

 **Example 1** 

```
Input 1: a maze represented by a 2D array

0 0 1 0 0
0 0 0 0 0
0 0 0 1 0
1 1 0 1 1
0 0 0 0 0

Input 2: start coordinate (rowStart, colStart) = (0, 4)
Input 3: destination coordinate (rowDest, colDest) = (4, 4)

Output: true
Explanation: One possible way is : left -> down -> left -> down -> right -> down -> right.
```

 **Example 2** 

```
Input 1: a maze represented by a 2D array

0 0 1 0 0
0 0 0 0 0
0 0 0 1 0
1 1 0 1 1
0 0 0 0 0

Input 2: start coordinate (rowStart, colStart) = (0, 4)
Input 3: destination coordinate (rowDest, colDest) = (3, 2)

Output: false
Explanation: There is no way for the ball to stop at the destination.
```

**Note:**

1. There is only one ball and one destination in the maze.
2. Both the ball and the destination exist on an empty space, and they will not be at the same position initially.
3. The given maze does not contain border (like the red rectangle in  the example pictures), but you could assume the border of the maze are  all walls.
4. The maze contains at least 2 empty spaces, and both the width and height of the maze won't exceed 100.



Both BFS and DFS are solutions. 

DFS: bitmap directions in every pos to avoid duplicate loop.

```java
class Solution {
    public boolean hasPath(int[][] maze, int[] start, int[] destination) {
        if(maze[start[0]][start[1]] == 1) return false;
        int[][] dirs = new int[maze.length][maze[0].length];
        int[][] vecs = {{-1, 0, 1}, {1, 0, 2}, {0, -1, 4}, {0, 1, 8}};
        
        for(int i = 0; i < 4; i++) {
            if( dfs(maze, dirs, vecs, start[0], start[1], i, destination))
                return true;
        }
        
        return false;
    }
    
    // 0 left, 1 right, 2 up, 3 down
    private boolean dfs(int[][] maze, int[][] dirs, int[][] vecs, int i, int j, int d, int[] destination) {
        if(i == destination[0] && j == destination[1]) return true;
        
        int dir = vecs[d][2];
        if((dir & dirs[i][j]) != 0) return false;
        
        dirs[i][j] |= dir;
        
        int dx = vecs[d][0], dy = vecs[d][1];
        
        i += dx; j += dy;
        
        while(i >= 0 && i < maze.length && j >= 0 && j < maze[0].length) {
            // hit a wall
            if(maze[i][j] == 1) break;
            
            // visted path
            if((dirs[i][j] & dir) != 0) return false;
            dirs[i][j] |= dir;
            i += dx; j += dy;
        }
        
        i -= dx; j -= dy;
        
        for(int k = 0; k < 2; k++) {
            if(dfs(maze, dirs, vecs, i, j, (d < 2 ? 2 : 0) + k, destination))
                return true;
        }
        
        return false;
    }
}
```

