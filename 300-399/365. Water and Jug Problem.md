# 365. Water and Jug Problem

You are given two jugs with capacities *x* and *y* litres. There is an infinite amount of water supply available. You need to determine whether it is possible to measure exactly *z* litres using these two jugs.

If *z* liters of water is measurable, you must have *z* liters of water contained within **one or both buckets** by the end.

Operations allowed:

- Fill any of the jugs completely with water.
- Empty any of the jugs.
- Pour water from one jug into another till the other jug is completely full or the first jug itself is empty.

**Example 1:** (From the famous [*"Die Hard"* example](https://www.youtube.com/watch?v=BVtQNK_ZUJg))

```
Input: x = 3, y = 5, z = 4
Output: True
```

**Example 2:**

```
Input: x = 2, y = 6, z = 5
Output: False
```



BFS solution:

```java
/*
* BFS solution, every state will have 4 children states: 
* val + x, val + y, val - x, val - y
* using set to trick visited problem
*
**/

class Solution {
    public boolean canMeasureWater(int x, int y, int z) {
        if(z < 0 || z > x + y) return false;
        
        HashSet<Integer> set = new HashSet<>();
        Queue<Integer> queue = new LinkedList<>();
        
        queue.offer(0);
        set.add(0);
        while(!queue.isEmpty()) {
            int cur = queue.poll();
            if(cur + x <= x + y && set.add(cur + x))
                queue.offer(cur + x);
            if(cur + y <= x + y && set.add(cur + y))
                queue.offer(cur + y);
            if(cur - x > 0 && set.add(cur - x))
                queue.offer(cur - x);
            if(cur - y > 0 && set.add(cur - y))
                queue.offer(cur - y);
            
            if(set.contains(z))
                return true;
        }
        
        return false;
    }
}
```



GCD solution

```java
public boolean canMeasureWater(int x, int y, int z) {
    //limit brought by the statement that water is finallly in one or both buckets
    if(x + y < z) return false;
    //case x or y is zero
    if( x == z || y == z || x + y == z ) return true;
    
    //get GCD, then we can use the property of Bézout's identity
    return z%GCD(x, y) == 0;
}

public int GCD(int a, int b){
    while(b != 0 ){
        int temp = b;
        b = a%b;
        a = temp;
    }
    return a;
}
```

Bézout's identity (also called Bézout's lemma) is a theorem in the elementary theory of numbers:

**let a and b be nonzero integers and let d be their greatest common divisor. Then there exist integers x and y such that ax+by=d**
**In addition, the greatest common divisor d is the smallest positive integer that can be written as ax + by **
**every integer of the form ax + by is a multiple of the greatest common divisor d.**



**pay attention of GCD function **

